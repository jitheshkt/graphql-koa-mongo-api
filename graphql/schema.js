const { GraphQLSchema, GraphQLObjectType, GraphQLString} = require('graphql');
const carGraphQLType = require('./cars');
const cars = require('../models/cars');

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        car: {
            type: carGraphQLType,
            args: {id: {type: GraphQLString}},
            resolve(parent, args) {
                return cars.findById(args.id)
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery
});