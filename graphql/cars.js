const graphql = require('graphql');
const {GraphQLObjectType, GraphQLString} = graphql;

const CarType = new GraphQLObjectType({
    name: 'Cars',
    fields: () => ({
        name: {type: GraphQLString},
        manufacturer: {type: GraphQLString},
        price: {type: GraphQLString}
    })
});

module.exports = CarType;